package com.iomis.SpringBootRestAPI;//Payment requlest class

public class PaymentRequest {


    private int userID ;
    private int price;
    private String goodsID;
    private double discount;

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getGoodsID() {
        return goodsID;
    }

    public void setGoodsID(String goodsID) {
        this.goodsID = goodsID;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    @Override
    public String toString(){
        return "{id : "+userID +", itemID : "+ goodsID + ", price: " + price + ", discount: "+ Double.toString(discount)+"}";
    }
}
