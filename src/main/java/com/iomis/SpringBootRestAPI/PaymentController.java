package com.iomis.SpringBootRestAPI;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/payment")
public class PaymentController {


    private static final String SUCCESS_STATUS = "success";
    private static final String ERROR_STATUS = "error";
    private static final int CODE_SUCCESS = 100;
    private static final int AUTH_FAILURE = 102;

    private List<PaymentRequest> paymentsObjects = new ArrayList<>();

   public List<String> payments = new ArrayList<String>(){{
       add("{1-userid, 2-itemid , 3- price , 4 - discount}");
   }};

    @GetMapping
    public List<PaymentRequest> showStatus(){
        return paymentsObjects;
    }


    @GetMapping("{id}")
    public PaymentRequest showStatus(@PathVariable int id){
        for(int i=0;i<paymentsObjects.size();i++){

            if(paymentsObjects.get(i).getUserID()==id){
                return paymentsObjects.get(i);
            }
        }
        return null;
    }

    @PostMapping
    public List<String> pay( @RequestBody PaymentRequest request) {

       paymentsObjects.add(request);

            int userId = request.getUserID();
            String itemId = request.getGoodsID();
            int price = request.getPrice();
            double discount = request.getDiscount();

            payments.add(toString(userId,itemId,price,discount));

        return payments;
    }

    @PutMapping({"{id}"})
    public PaymentRequest update(@PathVariable int id , @RequestBody PaymentRequest request ){
        PaymentRequest result;
        for(int i=0;i<paymentsObjects.size();i++){

            if(paymentsObjects.get(i).getUserID()==id){
                request.setUserID(id);
                return paymentsObjects.set(i,request);
            }
        }
        return null;
    }


    @DeleteMapping("{id}")
    public void delete(@PathVariable int id){
        for(int i=0;i<paymentsObjects.size();i++) {

            if (paymentsObjects.get(i).getUserID() == id) {
                paymentsObjects.remove(i);

            }
        }
    }



    public String toString(int id, String itemID , int price , double discount){
         return "{id : "+id +", itemID : "+ itemID + ", price: " + price + ", discount: "+ Double.toString(discount)+"}";
    }
}
